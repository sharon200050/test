import React from "react";
import { Container } from "../Common/Container";
import { LoadingContainer } from "../Common/LoadingContainer";
import { AlertContainer } from "../Common/AlertContainer";
import PostUrlForm from "./PostUrlForm";

export class PostUrlPage extends React.Component {
  constructor(props) {
    super(props);
  }
  getAlert() {
    return "Something went wrong - please try again latter";
  }

  render() {
    const { isLoading, error, startPost } = this.props;
    return (
      <Container className="col-md-6 col-md-offset-3">
        <h2>Add stream</h2>
        {error ? <AlertContainer>{this.getAlert()}</AlertContainer> : <div />}
        {isLoading ? (
          <LoadingContainer>Loading</LoadingContainer>
        ) : (
          <PostUrlForm
            handleSubmit={({url}) => {
              startPost(url);
            }}
          />
        )}
      </Container>
    );
  }
}
