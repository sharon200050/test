import React, { Component } from "react";

export default class UrlInput extends Component {
  render() {
    const { submitted, url, handleChange } = this.props;
    return (
      <div
        className={"form-group" + (submitted && !url ? " has-error" : "")}
      >
        <label htmlFor="password">Url</label>
        <input
          type="text"
          className="form-control"
          name="url"
          value={url}
          onChange={handleChange}
        />
        {submitted && !url && (
          <div className="help-block">Url should not be empty</div>
        )}
      </div>
    );
  }
}
