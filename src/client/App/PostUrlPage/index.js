import { connect } from "react-redux";
import { getPostUrlStatus } from "../../State/Selectors/Url";
import { PostUrlPage } from "./PostUrlPage";
import { thunkPostUrl } from "../../State/Actions/Url";

function mapStateToProps(state) {
  const { isLoading, invalid, error } = getPostUrlStatus(state);
  return { isLoading, invalid, error };
}

function mapDispatchToProps(dispatch) {
  return {
    startPost: url => dispatch(thunkPostUrl(url))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostUrlPage);
