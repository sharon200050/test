import React, { Component } from "react";
import UrlInput from "./UrlInput";

export default class PostUrlForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      url: "",
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.ifFormValid = this.isFormValid.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  isFormValid() {
    const { url } = this.state;
    return !!url;
  }

  render() {
    const { url, submitted } = this.state;
    const { handleSubmit } = this.props;

    return (
      <div>
        <UrlInput
          submitted={submitted}
          url={url}
          handleChange={this.handleChange}
        />
        <div className="form-group">
          <button
            className="btn btn-primary"
            onClick={() => {
              this.setState({ submitted: true });
              if (this.isFormValid()) {
                handleSubmit(this.state);
              }
            }}
          >
            Send
          </button>
        </div>
      </div>
    );
  }
}
