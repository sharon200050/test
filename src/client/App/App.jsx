import React, { Component } from "react";
import styled from "styled-components";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";
import PostUrlPage  from "./PostUrlPage";


export class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { username, isLoggedIn } = this.props;
    if(!isLoggedIn) {
      return <LoginPage />
    }

    return <div><PostUrlPage /></div>
    // return <div>
    //   <RegisterPage />
    // </div>
  }
}
