import React from "react";
import { LoginForm } from "./LoginForm";
import { Container } from "../Common/Container";
import { LoadingContainer } from "../Common/LoadingContainer";
import { AlertContainer } from "../Common/AlertContainer";

export class LoginPage extends React.Component {
  constructor(props) {
    super(props);
  }
  getAlert(invalid, error) {
    if (invalid) {
      return "You have entered wrong username of password!";
    }

    if (error) {
      return "Something went wrong - please try again latter";
    }
  }
  render() {
    const { isLoading, invalid, error, startLogin } = this.props;
    return (
      <Container className="col-md-6 col-md-offset-3">
        <div className="alert alert-info">
          Username: test
          <br />
          Password: test
        </div>

        <h2>Login</h2>
        {invalid || error ? (
          <AlertContainer>{this.getAlert(invalid, error)}</AlertContainer>
        ) : (
          <div />
        )}
        {isLoading ? (
          <LoadingContainer>Loading</LoadingContainer>
        ) : (
          <LoginForm
            handleSubmit={({ username, password }) => {
              startLogin({ username, password });
            }}
          />
        )}
      </Container>
    );
  }
}
