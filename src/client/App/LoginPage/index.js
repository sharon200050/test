import { connect } from "react-redux";
import { LoginPage } from "./LoginPage";
import { getLoginState } from "../../State/Selectors/User";
import { thunkLogin } from "../../State/Actions/User";

function mapStateToProps(state) {
  const { isLoading, invalid, error } = getLoginState(state);
  return { isLoading, invalid, error };
}

function mapDispatchToProps(dispatch) {
  return {
    startLogin: ({ username, password }) =>
      dispatch(thunkLogin({ username, password }))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
