import React from "react";
import { UsernameInput } from "../Common/UsernameInput";
import { PasswordInput } from "../Common/PasswordInput";

export class LoginForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.ifFormValid = this.isFormValid.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  isFormValid() {
    const { username, password } = this.state;
    return username && password;
  }

  render() {
    const { submitted, username, password } = this.state;
    const { handleSubmit } = this.props;

    return (
      <div>
        <UsernameInput
          submitted={submitted}
          username={username}
          handleChange={this.handleChange}
        />
        <PasswordInput
          submitted={submitted}
          password={password}
          handleChange={this.handleChange}
        />
        <div className="form-group">
          <button
            className="btn btn-primary"
            onClick={() => {
              this.setState({ submitted: true });
              if (this.isFormValid()) {
                handleSubmit(this.state);
              }
            }}
          >
            Login
          </button>
        </div>
      </div>
    );
  }
}
