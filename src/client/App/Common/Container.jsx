import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin: 1em;
  padding: 1em;
  border: 1px solid black;
  border-radius: 1em;
`;