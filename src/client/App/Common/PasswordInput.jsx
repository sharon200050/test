import React, { Component } from "react";

export class PasswordInput extends Component {
  render() {
    const { submitted, password, handleChange } = this.props;
    return (
      <div
        className={"form-group" + (submitted && !password ? " has-error" : "")}
      >
        <label htmlFor="password">Password</label>
        <input
          type="password"
          className="form-control"
          name="password"
          value={password}
          onChange={handleChange}
        />
        {submitted && !password && (
          <div className="help-block">Password is required</div>
        )}
      </div>
    );
  }
}
