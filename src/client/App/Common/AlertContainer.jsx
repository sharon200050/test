import styled from 'styled-components'

export const AlertContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  color: red;
`;