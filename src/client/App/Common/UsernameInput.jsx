import React, { Component } from "react";

export class UsernameInput extends Component {
  render() {
    const { submitted, username, handleChange } = this.props;
    return (
      <div
        className={"form-group" + (submitted && !username ? " has-error" : "")}
      >
        <label htmlFor="username">Username</label>
        <input
          type="text"
          className="form-control"
          name="username"
          value={username}
          onChange={handleChange}
        />
        {submitted && !username && (
          <div className="help-block">Username is required</div>
        )}
      </div>
    );
  }
}
