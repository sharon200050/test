import { connect } from "react-redux";
import { App } from "./App";
import { getUsername, isLoggedIn } from "../State/Selectors/User";

const mapStateToProps = state => {
  return {
    isLoggedIn: isLoggedIn(state),
    username: getUsername(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
