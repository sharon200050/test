import { connect } from "react-redux";
import { RegisterPage } from "./RegisterPage";
import { getRegisterState } from "../../State/Selectors/User";
import { thunkRegister } from "../../State/Actions/Register";

function mapStateToProps(state) {
  const { isLoading, invalid, error } = getRegisterState(state);
  return { isLoading, invalid, error };
}

function mapDispatchToProps(dispatch) {
  return {
    startRegister: ({ username, password }) =>
      dispatch(thunkRegister({ username, password }))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);
