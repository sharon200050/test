import React from "react";
import { RegisterForm } from "./RegisterForm";
import { Container } from "../Common/Container";
import { LoadingContainer } from "../Common/LoadingContainer";
import { AlertContainer } from "../Common/AlertContainer";


export class RegisterPage extends React.Component {
  constructor(props) {
    super(props);
  }
  getAlert(invalid, error) {
    if (invalid) {
      return "This username is not available";
    }

    if (error) {
      return "Something went wrong - please try again latter";
    }
  }
  render() {
    const { isLoading, invalid, error, startRegister } = this.props;
    return (
      <Container className="col-md-6 col-md-offset-3">
        <h2>Resgister</h2>
        {invalid || error ? (
          <AlertContainer>{this.getAlert(invalid, error)}</AlertContainer>
        ) : (
          <div />
        )}
        {isLoading ? (
          <LoadingContainer>Loading</LoadingContainer>
        ) : (
          <RegisterForm
            handleSubmit={({ username, password }) => {
              startRegister({ username, password });
            }}
          />
        )}
      </Container>
    );
  }
}
