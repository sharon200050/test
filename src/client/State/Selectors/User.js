export const getUsername = state => {
  const data = state.user.data;
  return data ? data.username : null;
};

export const isLoggedIn = state => {
  return !!state.user.data;
};

export const getLoginState = state => {
  const { isLoading, invalid, error } = state.user;
  return {
    isLoading,
    invalid,
    error
  };
};

export const getRegisterState = state => {
  const { isLoading, invalid, error } = state.register;
  return {
    isLoading,
    invalid,
    error
  };
};
