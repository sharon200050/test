import { Types } from "./Types/Types";
import { api } from "../../Api/index";

const RegisterTypes = Types.RegisterTypes;

export const thunkRegister = ({ username, password }) => {
  return async dispatch => {
    dispatch(registerStart());
    const result = await api.register({ username, password });
    if (result.error) {
      dispatch(registerError());
      return;
    }
    if (result.invalid) {
      dispatch(registerInvalid());
      return;
    }

    dispatch(registerDone(result.data));
  };
};

export const registerStart = () => {
  return {
    type: RegisterTypes.REGISTER_START
  };
};

export const registerDone = (data) => {
  return {
    type: RegisterTypes.REGISTER_DONE,
    data
  };
};

export const registerInvalid = () => {
  return {
    type: RegisterTypes.REGISTER_INVALID
  };
};

export const registerError = () => {
  return {
    type: RegisterTypes.REGISTER_ERROR
  };
};
