import { Types } from "./Types/Types";
import { api } from "../../Api/index";




export const thunkFetchProducts = () => {
  return async dispatch => {
    try {
      dispatch(fetchProducts());
      const products = await api.fetchProducts();
      dispatch(addProducts(products));
      dispatch(initAmount(products.map(p => p.barcode)));
      dispatch(receiveProducts());
    } catch (e) {
      dispatch(errorProducts());
    }
  };
};

export const fetchProducts = () => {
  return {
    type: Types.FETCH_PRODUCTS
  };
};

export const receiveProducts = () => {
  return {
    type: Types.RECEIVE_PRODUCTS
  };
};

export const errorProducts = () => {
  return {
    type: Types.ERROR_PRODUCTS
  };
};

export const addProducts = products => {
  return {
    type: Types.ADD_PRODUCTS,
    products
  };
};

export const incItem = barcode => {
  return {
    type: Types.INC_ITEM,
    barcode
  };
};

export const decItem = barcode => {
  return {
    type: Types.DEC_ITEM,
    barcode
  };
};

export const initAmount = barcodeArr => {
  return {
    type: Types.INIT_AMOUNT,
    barcodeArr
  };
};
