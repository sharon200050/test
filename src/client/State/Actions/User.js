import { Types } from "./Types/Types";
import { api } from "../../Api/index";

const UserTypes = Types.UserTypes;

export const thunkLogin = ({ username, password }) => {
  return async dispatch => {
    dispatch(loginStart());
    const result = await api.login({ username, password });
    if (result.error) {
      dispatch(loginError());
      return;
    }
    if (result.invalid) {
      dispatch(loginInvalid());
      return;
    }

    dispatch(setData(result.data));
  };
};

export const loginStart = () => {
  return {
    type: UserTypes.LOGIN_START
  };
};

export const loginInvalid = () => {
  return {
    type: UserTypes.LOGIN_INVALID
  };
};

export const loginError = () => {
  return {
    type: UserTypes.LOGIN_ERROR
  };
};

export const setData = ({ username, token }) => {
  return {
    type: UserTypes.SET_DATA,
    data: { username, token }
  };
};

export const clearData = () => {
  return {
    type: UserTypes.CLEAR_DATA
  };
};
