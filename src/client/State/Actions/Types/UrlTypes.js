export const UrlTypes = {
  ADD_URLS: "ADD_URLS",
  // Async urls fetch actions
  FETCH_START: "FETCH_START",
  FETCH_ERROR: "FETCH_ERROR",
  FETCH_DONE: "FETCH_DONE",
  // Async new url posting
  POST_START: "POST_START",
  POST_ERROR: "POST_ERROR",
  POST_DONE: "POST_DONE"
};
