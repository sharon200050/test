export const UserTypes = {
  // Should be called after login
  SET_DATA: "SET_DATA",
  // Should be called after logout
  CLEAR_DATA: "CLEAR_DATA",
  // Async login related actions
  LOGIN_START: "LOGIN_START",
  LOGIN_INVALID: "LOGIN_INVALID",
  LOGIN_ERROR: "LOGIN_ERROR"
};
