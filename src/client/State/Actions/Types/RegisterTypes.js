export const RegisterTypes = {
  // Async register related actions
  REGISTER_START: "REGISTER_START",
  REGISTER_DONE: "REGISTER_DONE",
  REGISTER_INVALID: "REGISTER_INVALID",
  REGISTER_ERROR: "REGISTER_ERROR"
};
