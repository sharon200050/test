import { UrlTypes } from "./UrlTypes";
import { RegisterTypes } from "./RegisterTypes";
import { UserTypes } from "./UserTypes";

export const Types = {
  UrlTypes,
  RegisterTypes,
  UserTypes
};
