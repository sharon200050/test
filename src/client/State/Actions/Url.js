import { Types } from "./Types/Types";
import { api } from "../../Api/index";

const UrlTypes = Types.UrlTypes;

export const thunkFetchUrls = () => {
  return async dispatch => {
    dispatch(fetchStart());
    const result = await api.fetchUrls();
    if (result.error) {
      dispatch(fetchError());
      return;
    }

    dispatch(addUrls({ data: result.data }));
    dispatch(fetchDone());
  };
};

export const thunkPostUrl = url => {
  return async dispatch => {
    dispatch(postStart());
    const result = await api.addUrl(url);
    if (result.error) {
      dispatch(postError());
      return;
    }

    dispatch(addUrls(result.data));
    dispatch(postDone())
  };
};

export const addUrls = data => {
  return {
    type: UrlTypes.ADD_URLS,
    data
  };
};

export const fetchStart = () => {
  return {
    type: UrlTypes.FETCH_START
  };
};

export const fetchDone = () => {
  return {
    type: UrlTypes.FETCH_DONE
  };
};

export const fetchError = () => {
  return {
    type: UrlTypes.FETCH_ERROR
  };
};

export const postStart = () => {
  return {
    type: UrlTypes.POST_START
  };
};

export const postDone = () => {
  return {
    type: UrlTypes.POST_DONE
  };
};

export const postError = () => {
  return {
    type: UrlTypes.POST_ERROR
  };
};
