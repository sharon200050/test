import { Types } from "../Actions/Types/Types";

const initialState = {
  isLoading: false,
  invalid: false,
  error: null,
  data: null
};

const RegisterTypes = Types.RegisterTypes;

export const register = (state = initialState, action) => {
  const { type, data } = action;

  switch (type) {
    case RegisterTypes.REGISTER_START:
      return {
        ...state,
        isLoading: true,
        invalid: false,
        error: null,
        data: null
      };

    case RegisterTypes.REGISTER_DONE:
      return {
        ...state,
        isLoading: false,
        invalid: false,
        error: null,
        data: data
      };

    case RegisterTypes.REGISTER_INVALID:
      return {
        ...state,
        isLoading: false,
        invalid: true,
        error: null,
        data: null
      };

    case RegisterTypes.REGISTER_ERROR:
      return {
        ...state,
        isLoading: false,
        invalid: false,
        error: true,
        data: null
      };
    default:
      return state;
  }
};
