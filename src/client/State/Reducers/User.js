import { Types }  from "../Actions/Types/Types";

const initialState = {
  isLoading: false,
  invalid: false,
  error: null,
  data: null
};

const UserTypes = Types.UserTypes

export const user = (state = initialState, action) => {
  const { type, data } = action;

  switch (type) {
    case UserTypes.SET_DATA:
      return {
        ...state,
        data
      };

    case UserTypes.CLEAR_DATA:
      return {
        ...state,
        data: null
      };

    case UserTypes.LOGIN_START:
      return {
        ...state,
        isLoading: true,
        invalid: false,
        error: null,
        data: null
      };

    case UserTypes.LOGIN_INVALID:
      return {
        ...state,
        isLoading: false,
        invalid: true,
        error: null,
        data: null
      };

    case UserTypes.LOGIN_ERROR:
      return {
        ...state,
        isLoading: false,
        invalid: false,
        error: true,
        data: null
      };
    default:
      return state;
  }
};
