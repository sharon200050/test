import { combineReducers } from "redux";
import { fetch } from "./Fetch";
import { post } from "./Post";
import { data } from "./Data";

export const url = combineReducers({ fetch, post, data });
