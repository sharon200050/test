import { Types } from "../../Actions/Types/Types";

const initialState = {
  isLoading: false,
  error: null
};

const UrlTypes = Types.UrlTypes;

export const post = (state = initialState, action) => {
  const { type } = action;

  switch (type) {
    case UrlTypes.POST_START:
      return {
        ...state,
        isLoading: true,
        error: null
      };

    case UrlTypes.POST_DONE:
      return {
        ...state,
        isLoading: false,
        error: null
      };

    case UrlTypes.POST_ERROR:
      return {
        ...state,
        isLoading: false,
        error: true
      };

    default:
      return state;
  }
};
