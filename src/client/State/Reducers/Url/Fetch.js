import { Types } from "../../Actions/Types/Types";

const initialState = {
  isLoading: false,
  error: null
};

const UrlTypes = Types.UrlTypes;

export const fetch = (state = initialState, action) => {
  const { type } = action;

  switch (type) {
    case UrlTypes.FETCH_START:
      return {
        ...state,
        isLoading: true,
        error: null
      };

    case UrlTypes.FETCH_DONE:
      return {
        ...state,
        isLoading: false,
        error: null
      };

    case UrlTypes.FETCH_ERROR:
      return {
        ...state,
        isLoading: false,
        error: true
      };

    default:
      return state;
  }
};
