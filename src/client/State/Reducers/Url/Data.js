import { Types } from "../../Actions/Types/Types";

const initialState = [];

const UrlTypes = Types.UrlTypes;

export const data = (state = initialState, action) => {
  const { type, data } = action;

  switch (type) {
    case UrlTypes.ADD_URLS:
      return [...state, ...data];

    default:
      return state;
  }
};
