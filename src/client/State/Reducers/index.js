import { combineReducers } from "redux";
import { user } from "./User";
import { register } from "./Register";
import { url } from "./Url";

export const rootReducer = combineReducers({ user, register, url });
