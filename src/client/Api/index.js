const delay = 4000;

// Should return {invalid, error, data} object
// invalid === true means invalid data
// error means internal server error, no connection to server etc.
// data means successful login
const login = async ({ username, password }) => {
  // return {
  //   invalid: false,
  //   error: true,
  //   data: null
  // };
  await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, delay);
  });

  if (username === "test" && password === "test") {
    return {
      invalid: false,
      error: null,
      data: {
        username: "username",
        token: "token"
      }
    };
  }

  return {
    invalid: true,
    error: null,
    data: null
  };
};

const register = async ({ username, password }) => {
  await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, delay);
  });

  if (username === "new" && password === "new") {
    return {
      invalid: false,
      error: null,
      data: {
        username: "new"
      }
    };
  }

  return {
    invalid: false,
    error: true,
    data: null
  };
};

const fetchUrls = async () => {
  await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, delay);
  });

  return {
    error: null,
    data: ["url1", "url2"]
  };
};

const addUrl = async url => {
  await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, delay);
  });

  return {
    error: null,
    data: [url]
  };
};
export const api = {
  login,
  register,
  fetchUrls,
  addUrl
};
