﻿const config = require("../config.json");
const jwt = require("jsonwebtoken");

// users hardcoded for simplicity, store in a db for production applications
let users = [
  {
    id: 1,
    username: "test",
    password: "test",
    urls: ["url1", "url2"]
  }
];

module.exports = {
  authenticate,
  getAll,
  register,
  addUrl
};

async function authenticate({ username, password }) {
  const user = users.find(
    u => u.username === username && u.password === password
  );
  if (user) {
    const token = jwt.sign({ sub: user.id }, config.secret);
    const { password, ...userWithoutPassword } = user;
    return {
      ...userWithoutPassword,
      token
    };
  }
}

async function getAll() {
  return users.map(u => {
    const { password, ...userWithoutPassword } = u;
    return userWithoutPassword;
  });
}

async function addUrl(username, url) {
  const userIndex = users.findIndex(user => user.username === username);
  if (userIndex < 0) {
    return false;
  }

  const user = users[userIndex];
  const userWithNewUrl = {
    ...user,
    urls: [...user.urls, url]
  };

  users[userIndex] = userWithNewUrl;
  return userWithNewUrl;
}

async function register({ username, password }) {
  const newUser = { id: users.length, username, password, urlArr: [] };
  users = [...users, newUser];
  const userWithoutPassword = { id: newUser.id, username: newUser.username };
  return userWithoutPassword;
}
