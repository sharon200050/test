﻿const express = require("express");
const router = express.Router();
const userService = require("./service");

// routes
router.post("/authenticate", authenticate);
router.post("/register", register);
router.post("/addurl", addUrl);
router.get("/", getAll);

module.exports = router;

function register(req, res, next) {
  userService.register(req.body).then(result => res.send(result));
}

function addUrl(req, res, next) {
  userService
    .addUrl(req.body.username, req.body.url)
    .then(result => {
      result
        ? res.send(result)
        : res.status(400).json({ message: "User doesn't exist" });
    })
    .catch(err => next(err));
}

function authenticate(req, res, next) {
  userService
    .authenticate(req.body)
    .then(user =>
      user
        ? res.json(user)
        : res.status(400).json({ message: "Username or password is incorrect" })
    )
    .catch(err => next(err));
}

function getAll(req, res, next) {
  userService
    .getAll()
    .then(users => res.json(users))
    .catch(err => next(err));
}
